var Ajedrez = (function() {

  var _llenaTablero=function(archivo){
    var este_tablero = document.getElementById("tablero");

    este_tablero.setAttribute("align","center");
    var table = crear_elemento ("table",{"id":"ajedrez"},{});
    table.setAttribute("style", "border : 2px solid;");
    table.setAttribute("id","table");

    var thead=document.createElement("thead");
    var tbody=document.createElement("tbody");
    var lineas = archivo.split("\n");
    var trCabecera=document.createElement("tr");
    var tdCab= document.createElement("td");
    tdCab.textContent="#";
    trCabecera.appendChild(tdCab);
    var cabeceras=lineas[0].split("|");
    for(var i=0;i<cabeceras.length;i++){
      var tdx=document.createElement("td");
      tdx.textContent=cabeceras[i];
      trCabecera.appendChild(tdx);
    }
    thead.appendChild(trCabecera);
    table.appendChild(thead);
        for(var i = 1; i < lineas.length-1; i++){
          var tr=document.createElement("tr");
            var fila=lineas[i].split("|");
            var tdNum=document.createElement("td");
            tdNum.textContent=i;
            tr.appendChild(tdNum);
              for(var j=0;j<fila.length;j++){
              var td=document.createElement("td");
                //var
                td.textContent=fila[j];
                td.setAttribute("id",cabeceras[j]+i);
                tr.appendChild(td);
              }
              tbody.appendChild(tr);
          }
      table.appendChild(tbody);
    este_tablero.appendChild(table);
  };

  var _cargaArchivo=function(){ //funcion que hace la solicitud del archivo tablero.csv al servidor
    const url ="csv/tablero.csv";
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      if (this.readyState === 4) {
        var mensaje = document.getElementById("mensaje");
        mensaje.textContent = "";
        if (this.status === 200){
        //  console.log(xhr.responseText);
          _llenaTablero(xhr.responseText);
        } else {
            mensaje.textContent = "Error " + this.status + " " + this.statusText + " - " + this.responseURL;
        }
      }
    };
    xhr.open('GET', url, true);
    xhr.setRequestHeader('Access-Control-Allow-Headers', '*');
    xhr.send();
  };

  var _mostrar_tablero = function() {
    _cargaArchivo();
  };

  var _actualizar_tablero= function() {
    var tablita=document.getElementById('table');
    var padre=document.getElementById('tablero');
    padre.removeChild(tablita);
  _mostrar_tablero();
};
var agregaButton=function() {
  var padre=document.getElementById('opcion');
  var boton=document.createElement("input");
  boton.setAttribute("type","button");
  boton.setAttribute("id","actualizar");
  boton.setAttribute("value","Actualizar");
  boton.onclick=_actualizar_tablero;
  padre.appendChild(boton);
};

var _mover_pieza = function (movimientos){
    este = document.getElementById(movimientos.de);
    aqui = document.getElementById(movimientos.a);
    aqui.textContent = este.textContent;
    este.textContent= "";
}




agregaButton();

var crear_elemento = function(str_etiqueta, obj_atributos, obj_propiedades){
    var nuevo_elemento = document.createElement(str_etiqueta);
    for(var llave in obj_atributos){
        nuevo_elemento.setAttribute(llave, obj_atributos[llave]);
    }

    for(var llave in obj_propiedades){
        nuevo_elemento[llave] = obj_propiedades[llave];
    }
    return nuevo_elemento;
}

return {"mostrarTablero": _mostrar_tablero,"actualizarTablero": _actualizar_tablero,"moverPieza": _mover_pieza}
})();
